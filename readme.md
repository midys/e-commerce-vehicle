
  <a href="https://midys.web.id/">
    <img align="right" alt="midys" title="Midys Project" src="https://midys.web.id/assets/images/PROFILE.png" width="100" >
  </a>


<h2> 📁 Adiwerna Motor Website </h2>
<h6>By Midnight Studio.</h6>
<img align="right" alt="GIF" src="https://raw.githubusercontent.com/devSouvik/devSouvik/master/gif3.gif" width="300"/>

<h3> 👨🏻‍💻 Tentang Project Ini </h3>

-  Project ini berisi tentang website jual beli kendaraan. 


<h3>🛠 Tech Stack</h3>

- 💻 &nbsp; PHP | HTML | CSS | JavaScript | 
- 🛢 &nbsp; MySQL | Xampp
- 🔧 &nbsp; Visual Studio code | Git
- 🖥 &nbsp; Illustrator | Photoshop 

</br>



<h3> 🤝🏻 Connect with Me </h3>

<p align="center">
&nbsp; <a href="https://twitter.com" target="_blank" rel="noopener noreferrer"><img src="https://img.icons8.com/plasticine/100/000000/twitter.png" width="50" /></a>  
&nbsp; <a href="https://www.instagram.com/mdc.6930" target="_blank" rel="noopener noreferrer"><img src="https://img.icons8.com/plasticine/100/000000/instagram-new.png" width="50" /></a>  
&nbsp; <a href="https://www.linkedin.com/" target="_blank" rel="noopener noreferrer"><img src="https://img.icons8.com/plasticine/100/000000/linkedin.png" width="50" /></a>
&nbsp; <a href="mailto:mcprr25@gmail.com" target="_blank" rel="noopener noreferrer"><img src="https://img.icons8.com/plasticine/100/000000/gmail.png"  width="50" /></a>
</p>

⭐️ From [Midnight Studio](https://midys.web.id)

